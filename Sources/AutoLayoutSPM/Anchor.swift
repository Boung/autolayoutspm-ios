//
//  UIView + Extension.swift
//  
//
//  Created by Ly Boung on 17/8/23.
//

import UIKit

public extension UIView {
  enum PriorityType {
    case equal
    case lessThanOrEqual
    case greaterThanOrEqaul
  }
  
  func layoutView(handler: (UIView) -> Void) {
    toggleTranslatesAutoresizingMaskIntoConstraints()
    handler(self)
  }
  
  /// Anchor view to superView
  /// - Parameters:
  ///   - padding: Padding for each anchor
  ///   - toSafeArea: Bool value for anchor to safeArea
  /// - Returns: Current view
  @discardableResult
  func fill(padding: UIEdgeInsets = .zero, toSafeArea: Bool = true) -> Self {
    toggleTranslatesAutoresizingMaskIntoConstraints()
    
    if toSafeArea {
      topAnchor.constraint(equalTo: superview.unsafelyUnwrapped.layoutMarginsGuide.topAnchor, constant: padding.top).isActive = true
      bottomAnchor.constraint(equalTo: superview.unsafelyUnwrapped.layoutMarginsGuide.bottomAnchor, constant: -padding.bottom).isActive = true
      leadingAnchor.constraint(equalTo: superview.unsafelyUnwrapped.leadingAnchor, constant: padding.left).isActive = true
      trailingAnchor.constraint(equalTo: superview.unsafelyUnwrapped.trailingAnchor, constant: -padding.right).isActive = true
    }else {
      topAnchor.constraint(equalTo: superview.unsafelyUnwrapped.topAnchor, constant: padding.top).isActive = true
      bottomAnchor.constraint(equalTo: superview.unsafelyUnwrapped.bottomAnchor, constant: -padding.bottom).isActive = true
      leadingAnchor.constraint(equalTo: superview.unsafelyUnwrapped.leadingAnchor, constant: padding.left).isActive = true
      trailingAnchor.constraint(equalTo: superview.unsafelyUnwrapped.trailingAnchor, constant: -padding.right).isActive = true
    }
    
    return self
  }
  
  /// Anchor view's topAnchor to view
  /// - Parameters:
  ///   - constraint: View's anchor to attach to. If Nil, will anchor to superView.topAnchor
  ///   - constant: Padding Constant
  ///   - priority: Anchor Priority (.equal / .lessThanOrEqual / .greaterThanOrEqaul)
  /// - Returns: Current view
  @discardableResult
  func top(constraint: NSLayoutYAxisAnchor? = nil, constant: CGFloat = 0.0, priority: PriorityType = .equal) -> Self {
    let constraint = constraint == nil ? superview.unsafelyUnwrapped.topAnchor : constraint.unsafelyUnwrapped
    toggleTranslatesAutoresizingMaskIntoConstraints()
    
    switch priority {
      case .equal:
        topAnchor.constraint(equalTo: constraint, constant: constant).isActive = true
      case .lessThanOrEqual:
        topAnchor.constraint(lessThanOrEqualTo: constraint, constant: constant).isActive = true
      case .greaterThanOrEqaul:
        topAnchor.constraint(greaterThanOrEqualTo: constraint, constant: constant).isActive = true
    }
    
    return self
  }
  
  /// Anchor view's bottomAnchor to view
  /// - Parameters:
  ///   - constraint: View's anchor to attach to. If Nil, will anchor to superView.bottomAnchor
  ///   - constant: Padding Constant
  ///   - priority: Anchor Priority (.equal / .lessThanOrEqual / .greaterThanOrEqaul)
  /// - Returns: Current view
  @discardableResult
  func bottom(constraint: NSLayoutYAxisAnchor? = nil, constant: CGFloat = 0.0, priority: PriorityType = .equal) -> Self {
    let constraint = constraint == nil ? superview.unsafelyUnwrapped.bottomAnchor : constraint.unsafelyUnwrapped
    toggleTranslatesAutoresizingMaskIntoConstraints()
    
    switch priority {
      case .equal:
        bottomAnchor.constraint(equalTo: constraint, constant: -constant).isActive = true
      case .lessThanOrEqual:
        bottomAnchor.constraint(lessThanOrEqualTo: constraint, constant: -constant).isActive = true
      case .greaterThanOrEqaul:
        bottomAnchor.constraint(greaterThanOrEqualTo: constraint, constant: -constant).isActive = true
    }
    
    return self
  }
  
  /// Anchor view's leadingAnchor to view
  /// - Parameters:
  ///   - constraint: View's anchor to attach to. If Nil, will anchor to superView.leadingAnchor
  ///   - constant: Padding Constant
  ///   - priority: Anchor Priority (.equal / .lessThanOrEqual / .greaterThanOrEqaul)
  /// - Returns: Current view
  @discardableResult
  func leading(constraint: NSLayoutXAxisAnchor? = nil, constant: CGFloat = 0.0, priority: PriorityType = .equal) -> Self {
    let constraint = constraint == nil ? superview.unsafelyUnwrapped.leadingAnchor : constraint.unsafelyUnwrapped
    toggleTranslatesAutoresizingMaskIntoConstraints()
    
    switch priority {
      case .equal:
        leadingAnchor.constraint(equalTo: constraint, constant: constant).isActive = true
      case .lessThanOrEqual:
        leadingAnchor.constraint(lessThanOrEqualTo: constraint, constant: constant).isActive = true
      case .greaterThanOrEqaul:
        leadingAnchor.constraint(greaterThanOrEqualTo: constraint, constant: constant).isActive = true
    }
    
    return self
  }
  
  /// Anchor view's trailingAnchor to view
  /// - Parameters:
  ///   - constraint: View's anchor to attach to. If Nil, will anchor to superView.trailingANchor
  ///   - constant: Padding Constant
  ///   - priority: Anchor Priority (.equal / .lessThanOrEqual / .greaterThanOrEqaul)
  /// - Returns: Current view
  @discardableResult
  func trailing(constraint: NSLayoutXAxisAnchor? = nil, constant: CGFloat = 0.0, priority: PriorityType = .equal) -> Self {
    let constraint = constraint == nil ? superview.unsafelyUnwrapped.trailingAnchor : constraint.unsafelyUnwrapped
    toggleTranslatesAutoresizingMaskIntoConstraints()
    
    switch priority {
      case .equal:
        trailingAnchor.constraint(equalTo: constraint, constant: -constant).isActive = true
      case .lessThanOrEqual:
        trailingAnchor.constraint(lessThanOrEqualTo: constraint, constant: -constant).isActive = true
      case .greaterThanOrEqaul:
        trailingAnchor.constraint(greaterThanOrEqualTo: constraint, constant: -constant).isActive = true
    }
    
    return self
  }
  
  /// Anchor view's widthAnchor
  /// - Parameters:
  ///   - constant: Size for width
  ///   - priority: Anchor Priority (.equal / .lessThanOrEqual / .greaterThanOrEqaul)
  /// - Returns: Current view
  @discardableResult
  func width(constant: CGFloat = 0.0, priority: PriorityType = .equal) -> Self {
    toggleTranslatesAutoresizingMaskIntoConstraints()
    
    switch priority {
      case .equal:
        widthAnchor.constraint(equalToConstant: constant).isActive = true
      case .lessThanOrEqual:
        widthAnchor.constraint(lessThanOrEqualToConstant: constant).isActive = true
      case .greaterThanOrEqaul:
        widthAnchor.constraint(greaterThanOrEqualToConstant: constant).isActive = true
    }
    
    return self
  }
  
  /// Anchor view's heightAnchor
  /// - Parameters:
  ///   - constant: Size for height
  ///   - priority: Anchor Priority (.equal / .lessThanOrEqual / .greaterThanOrEqaul)
  /// - Returns: Current view
  @discardableResult
  func height(constant: CGFloat = 0.0, priority: PriorityType = .equal) -> Self {
    toggleTranslatesAutoresizingMaskIntoConstraints()
    
    switch priority {
      case .equal:
        heightAnchor.constraint(equalToConstant: constant).isActive = true
      case .lessThanOrEqual:
        heightAnchor.constraint(lessThanOrEqualToConstant: constant).isActive = true
      case .greaterThanOrEqaul:
        heightAnchor.constraint(greaterThanOrEqualToConstant: constant).isActive = true
    }
    
    return self
  }
  
  /// Anchor view's center to view
  /// - Parameters:
  ///   - view: View to anchor to. If nil, will anchor to superView.centerXAnchor and superView.centerYAnchor
  ///   - xConstant: Constant for xAnchor
  ///   - yConstant: Constant for yAnchor
  /// - Returns: Current view
  @discardableResult
  func center(to view: UIView? = nil, xConstant: CGFloat = 0.0, yConstant: CGFloat = 0.0) -> Self {
    toggleTranslatesAutoresizingMaskIntoConstraints()
    
    let xAnchor = view == nil ? superview.unsafelyUnwrapped.centerXAnchor : view.unsafelyUnwrapped.centerXAnchor
    let yAnchor = view == nil ? superview.unsafelyUnwrapped.centerYAnchor : view.unsafelyUnwrapped.centerYAnchor
    
    centerXAnchor.constraint(equalTo: xAnchor, constant: xConstant).isActive = true
    centerYAnchor.constraint(equalTo: yAnchor, constant: yConstant).isActive = true
    
    return self
  }
  
  /// Anchor view's center to view
  /// - Parameters:
  ///   - view: View to anchor to. If nil, will anchor to  superView.centerYAnchor
  ///   - constant: Constant for yAnchor
  /// - Returns: Current view
  @discardableResult
  func centerY(to view: UIView? = nil, constant: CGFloat = 0.0) -> Self {
    toggleTranslatesAutoresizingMaskIntoConstraints()
    
    let yAnchor = view == nil ? superview.unsafelyUnwrapped.centerYAnchor : view.unsafelyUnwrapped.centerYAnchor
    centerYAnchor.constraint(equalTo: yAnchor, constant: constant).isActive = true
    
    return self
  }
  
  /// Anchor view's center to view
  /// - Parameters:
  ///   - view: View to anchor to. If nil, will anchor to  superView.centerXAnchor
  ///   - constant: Constant for xAnchor
  /// - Returns: Current view
  @discardableResult
  func centerX(to view: UIView? = nil, constant: CGFloat = 0.0) -> Self {
    toggleTranslatesAutoresizingMaskIntoConstraints()
    
    let xAnchor = view == nil ? superview.unsafelyUnwrapped.centerXAnchor : view.unsafelyUnwrapped.centerXAnchor
    centerXAnchor.constraint(equalTo: xAnchor, constant: constant).isActive = true
    
    return self
  }
  
  private func toggleTranslatesAutoresizingMaskIntoConstraints() {
    if translatesAutoresizingMaskIntoConstraints {
      translatesAutoresizingMaskIntoConstraints = false
    }
  }
}
