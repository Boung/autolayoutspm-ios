//
//  Rendering.swift
//  
//
//  Created by Boung on 18/8/23.
//

import UIKit

public extension UIView {
  /// Render RoundView for view
  func roundView() {
    layoutIfNeeded()
    clipsToBounds = true
    
    if #available(iOS 13.0, *) {
      layer.cornerCurve = .continuous
    }
    layer.cornerRadius = frame.width / 2
  }
  
  /// Render CornerRadius for view
  /// - Parameters:
  ///   - corners: Desired corner to render radius (.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner)
  ///   - constant: Corner radius's value
  func setCornerRadiusOnly(corners: CACornerMask, constant: CGFloat = 0.0) {
    layoutIfNeeded()
    clipsToBounds = true
    layer.cornerRadius = constant
    layer.maskedCorners = corners
  }
  
  /// Render CornerRadius for view
  /// - Parameters:
  ///   - constant: Corner radius's value
  func setAllCornerRadius(constant: CGFloat) {
    layoutIfNeeded()
    clipsToBounds = true
    layer.cornerRadius = constant
    layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
  }
  
  /// Render border's width for view
  /// - Parameters:
  ///   - width: Border's width
  ///   - color: Border's color
  func setBorder(width: CGFloat = 0, color: UIColor = .white) {
    layoutIfNeeded()
    
    if #available(iOS 13.0, *) {
      layer.cornerCurve = .continuous
    }
    layer.borderWidth = width
    layer.borderColor = color.cgColor
  }
}
