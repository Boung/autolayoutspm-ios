//
//  Customization.swift
//  
//
//  Created by Boung on 17/8/23.
//

import Foundation

public protocol Customization { }

extension NSObject: Customization { }
extension Customization where Self: NSObject {
  
  /// - Description: Use to customize view properties
  /// - Parameters:
  ///   - closure: Callback function that return view back for customization.
  /// - Returns: Current View
  public func customize(closure: (Self) -> Void) -> Self {
    closure(self)
    return self
  }
}
