<h1>AutoLayoutSPM</h1>
There are some UIView Extension included on this SPM
<ul>
  <li> Customization function to customize view properties </li>
  <li> Autolayout constraint </li>
  <li> Rendering function </li>
</ul>

<h2>1. Customize</h2>
<h3>Function</h3>

```
func customize(closure: (Self) -> Void) -> Self { }
```

<h3>Usage</h3>

```
lazy var button = UIButton().customize {
  $0.setTitle("Click Me", for: .normal)
  $0.setTitleColor(.white, for: .normal)
  $0.backgroundColor = .orange
}
```

<h2>2. AutoLayout</h2>
<h3>Function</h3

```
func layoutView(handler: (UIView) -> Void) { }

func fill(padding: UIEdgeInsets = .zero, toSafeArea: Bool = true) -> Self { }

func top(constraint: NSLayoutYAxisAnchor? = nil, constant: CGFloat = 0.0, priority: PriorityType = .equal) -> Self { }

func bottom(constraint: NSLayoutYAxisAnchor? = nil, constant: CGFloat = 0.0, priority: PriorityType = .equal) -> Self { }

func leading(constraint: NSLayoutXAxisAnchor? = nil, constant: CGFloat = 0.0, priority: PriorityType = .equal) -> Self { }

func trailing(constraint: NSLayoutXAxisAnchor? = nil, constant: CGFloat = 0.0, priority: PriorityType = .equal) -> Self { }

func width(constant: CGFloat = 0.0, priority: PriorityType = .equal) -> Self { }

func height(constant: CGFloat = 0.0, priority: PriorityType = .equal) -> Self { }

func center(to view: UIView? = nil, xConstant: CGFloat = 0.0, yConstant: CGFloat = 0.0) -> Self { }

func centerY(to view: UIView? = nil, constant: CGFloat = 0.0) -> Self { }

func centerX(to view: UIView? = nil, constant: CGFloat = 0.0) -> Self { }
```

<h3>Usage</h3>

```
// Sample Button View with the use of customize function
lazy var button = UIButton().customize {
  $0.setTitle("Click Me", for: .normal)
  $0.setTitleColor(.white, for: .normal)
  $0.backgroundColor = .orange
}

// Applying constraint with above function

button.layoutView {
  view.addSubView($0)
  $0.top(constraint: view.layoutMarginsGuide.topAnchor, constant: 16, priority: .equal)
  $0.leading(priority: .greaterThanOrEqaul)
  $0.trailing()
  $0.height(constant: 100)
  $0.width(constant: 100)
}

```



<h2>3. Rendering</h2>
<h3>Function</h3>

```
func roundView() { }

func setCornerRadiusOnly(corners: UIRectCorner, constant: CGFloat = 0.0) { }

func drawBorder(border width: CGFloat = 0, color: UIColor = .white) { }
```

<h3>Usage</h3>

```
// Sample Button View with the use of customize function
lazy var button = UIButton().customize {
  $0.setTitle("Click Me", for: .normal)
  $0.setTitleColor(.white, for: .normal)
  $0.backgroundColor = .orange
}

// Round View
button.roundView()

// Corder Radius
button.setCornerRadiusOnly(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight, .allCorners], constant: 16)

// Draw Border
button.setBorder(width: 1, color: .gray)
```

<h1> Demo Project </h1>
Please refer to this <a href="https://gitlab.com/Boung/demoautolayoutspm">Demo Project</a>.
